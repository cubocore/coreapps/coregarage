/*
    *
    * This file is a part of CoreGarage.
    * A setting manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QDir>
#include <QFile>
#include <QIcon>
#include <QFileDialog>
#include <QWidget>
#include <QMessageBox>
#include <QFontDialog>
#include <QScroller>
#include <QPluginLoader>
#include <QTimer>

#include <cprime/applicationdialog.h>
#include <cprime/appopenfunc.h>
#include <cprime/cplugininterface.h>
#include <cprime/messageengine.h>
#include <cprime/systemxdg.h>
#include <cprime/themefunc.h>
#include <cprime/filefunc.h>

#include "coregarage.h"
#include "ui_coregarage.h"


coregarage::coregarage(QWidget *parent)
	: QWidget(parent)
    , ui(new Ui::coregarage)
    , smi(new settings)
    , autoDetect(true)
    , uiMode(0)
    , toolsIconSize(24, 24)
    , listViewIconSize(24, 24)
{
    ui->setupUi(this);

    QPalette pltt(palette());
    pltt.setColor(QPalette::Base, pltt.color(QPalette::Window));
    setPalette(pltt);

    initialize();
}

coregarage::~coregarage()
{
    smi->setValue("CoreGarage", "WindowSize", this->size());
    smi->setValue("CoreGarage", "WindowMaximized", this->isMaximized());

    delete smi;
    delete ui;
}

void coregarage::initialize()
{
    loadSettings();
    startSetup();
    setupIcons();

    setupCoreAppsPage();
    setupCoreActionPage();
    setupCoreTerminalPage();
    setupCoreStuffPage();

    on_general_clicked();
}

/**
 * @brief Setup ui elements
 */
void coregarage::startSetup()
{
    // all toolbuttons icon size in sideView
    QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
    Q_FOREACH (auto b, toolBtns) {
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }

    on_autoDetect_clicked(autoDetect);

    ui->menu->setVisible(false);
    ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);
    this->resize(800, 500);


    if (uiMode == 2) {
        // setup mobile UI

        this->setWindowState(Qt::WindowMaximized);
        ui->menu->setIconSize(toolsIconSize);
        ui->menu->setVisible(true);
        ui->menu->setChecked(false);
        ui->sideView->setVisible(false);

        QScroller::grabGesture(ui->scrollArea_2, QScroller::LeftMouseButtonGesture);
        QScroller::grabGesture(ui->scrollArea_3, QScroller::LeftMouseButtonGesture);
        QScroller::grabGesture(ui->scrollArea_4, QScroller::LeftMouseButtonGesture);
        QScroller::grabGesture(ui->scrollArea_5, QScroller::LeftMouseButtonGesture);
        QScroller::grabGesture(ui->pluginPaths, QScroller::LeftMouseButtonGesture);
        QScroller::grabGesture(ui->pluginsList, QScroller::LeftMouseButtonGesture);

    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }
    }

    connect(ui->limitHistorySize, &QGroupBox::toggled, this, [=](bool checked) {
        if (checked) {
            ui->historySB->setValue(-1);
        } else {
            ui->historySB->setValue(smi->getValue("CoreTerminal", "HistorySize", 500));
        }
    });

}

void coregarage::setupIcons()
{
    ui->ok->setIcon(CPrime::ThemeFunc::themeIcon( "object-select-symbolic", "object-select", "object-select" ));
    ui->restoreDefault->setIcon(CPrime::ThemeFunc::themeIcon( "edit-undo-symbolic", "edit-undo", "edit-undo" ));
    ui->cancel->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
}

/**
 * @brief Loads application settings
 */
void coregarage::loadSettings()
{
    // get CSuite's settings
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    autoDetect = smi->getValue("CoreApps", "AutoDetect");

    // get app's settings
    windowSize = smi->getValue("CoreGarage", "WindowSize");
    windowMaximized = smi->getValue("CoreGarage", "WindowMaximized");
}

void coregarage::setupCoreAppsPage()
{
    // general
    ui->isRecentDisable->setChecked(smi->getValue("CoreApps", "KeepActivities"));
    ui->listViewIconSize->setCurrentText(QString::number(QSize(smi->getValue("CoreApps", "ListViewIconSize")).width()));
    ui->iconViewIconSize->setCurrentText(QString::number(QSize(smi->getValue("CoreApps", "IconViewIconSize")).width()));
    ui->toolsIconSize->setCurrentText(QString::number(QSize(smi->getValue("CoreApps", "ToolsIconSize")).width()));
    ui->uiMode->setCurrentIndex(smi->getValue("CoreApps", "UIMode"));
    ui->autoDetect->setChecked(smi->getValue("CoreApps", "AutoDetect"));
    ui->disableTrashConfirmationMessage->setChecked(smi->getValue("CoreApps","DisableTrashConfirmationMessage"));

    // Preferred Applications
    QString temp = CPrime::SystemDefaultApps::getDefaultApp(CPrime::Terminal);
    ui->terminals->setText(temp.length() ? temp : "Nothing");

    temp = CPrime::SystemDefaultApps::getDefaultApp(CPrime::FileManager);
    ui->fileManger->setText(temp.length() ? temp : "Nothing");

    temp = CPrime::SystemDefaultApps::getDefaultApp(CPrime::ImageEditor);
    ui->imageEditor->setText(temp.length() ? temp : "Nothing");

    temp = CPrime::SystemDefaultApps::getDefaultApp(CPrime::MetadataViewer);
    ui->metadataViewer->setText(temp.length() ? temp : "Nothing");

    temp = CPrime::SystemDefaultApps::getDefaultApp(CPrime::SearchApp);
    ui->searchApp->setText(temp.length() ? temp : "Nothing");

    temp = CPrime::SystemDefaultApps::getDefaultApp(CPrime::BatchRenamer);
    ui->renamerApp->setText(temp.length() ? temp : "Nothing");

    // Advanced
    ui->experimentalFeatures->setChecked(smi->getValue("CoreApps", "EnableExperimental"));
}

void coregarage::setupCoreActionPage()
{
	loadPluginsPaths();
	loadPlugins();
}

void coregarage::setupCoreStuffPage()
{
    // CoreStuff
    ui->bgPath->setText(smi->getValue("CoreStuff", "Background"));
    ui->wallpaperPos->setCurrentIndex(smi->getValue("CoreStuff", "WallpaperPositon"));

    ui->keySequenceEdit->setKeySequence( QKeySequence::fromString( smi->getValue( "CoreStuff", "DockKeyCombo" ) ) );
}

void coregarage::setupCoreTerminalPage()
{
    /* Color Schemes */
    QStringList filters = QStringList() << "*.schema" << "*.colorscheme";

    ui->colorSchemesCB->clear();
    Q_FOREACH (QString schema, QDir("/usr/share/qtermwidget5/color-schemes/").entryList(filters, QDir::Files, QDir::Name | QDir::IgnoreCase)) {
        ui->colorSchemesCB->addItem(schema.replace(".schema", "").replace(".colorscheme", ""));
    }

    ui->colorSchemesCB->setCurrentText(smi->getValue("CoreTerminal", "ColorScheme"));

    /* Opacity */
    ui->opacitySB->setValue(smi->getValue("CoreTerminal", "Opacity"));

    /* Font */
    QFont tFont = smi->getValue("CoreTerminal", "Font");
//    ui->terminalFont->setFont(tFont);
    ui->terminalFont->setText(tFont.family() + ", " + QString::number(tFont.pointSize()));

    /* Shell program */
    ui->shellLE->setText(smi->getValue("CoreTerminal", "Shell"));

    ui->keytabCB->clear();
    /* Keytabs */
    QStringList filters2 = QStringList() << "*.keytab";
    Q_FOREACH (QString schema, QDir("/usr/share/qtermwidget5/kb-layouts/").entryList(filters2, QDir::Files, QDir::Name | QDir::IgnoreCase)) {
        ui->keytabCB->addItem(schema.replace(".keytab", ""));
    }

    ui->keytabCB->setCurrentText(smi->getValue("CoreTerminal", "KeyTab"));
    ui->cursorShapeCB->clear();
    ui->cursorShapeCB->addItems(QStringList() << "Block Cursor" << "Underline Cursor" << "IBeam Cursor");
    ui->cursorShapeCB->setCurrentIndex(smi->getValue("CoreTerminal", "CursorShape"));

	QString termEnv = smi->getValue("CoreTerminal", "TERM");
    if (!termEnv.length())
		termEnv = "xterm-256color";

	ui->termEnv->setCurrentText(termEnv);

	int historySize = smi->getValue("CoreTerminal", "HistorySize");

	if (historySize < 0) {
		ui->limitHistorySize->setChecked(false);
		historySize = -1;
	}
	ui->historySB->setValue(historySize);

	ui->sizeRowSB->setValue(smi->getValue("CoreTerminal", "Rows", 30));
	ui->sizeColSB->setValue(smi->getValue("CoreTerminal", "Columns", 120));
}


void coregarage::saveSettings()
{
    // coreapps
    bool rd(ui->isRecentDisable->isChecked());
    smi->setValue("CoreApps", "KeepActivities", rd);

    QString afp = CPrime::Variables::CC_ActivitiesFilePath();
    QString safp = QDir(CPrime::Variables::CC_Library_ConfigDir()).filePath("searchactivity");

    qDebug() << "CSuite recent activites file " << afp;
    qDebug() << "CoreHunt search activities file " << safp;

    if (not rd) {
        if (QFileInfo::exists(CPrime::Variables::CC_ActivitiesFilePath())){
            if (not QFile::remove(CPrime::Variables::CC_ActivitiesFilePath())){
                qDebug() << "failed to remove";
            }
        }

        if (QFileInfo::exists(afp)){
            if (not QFile::remove(afp)){
                qDebug() << "failed to remove";
            }
        }
    }
    smi->setValue("CoreApps", "EnableExperimental", ui->experimentalFeatures->isChecked());

    smi->setValue("CoreApps", "UIMode", ui->uiMode->currentIndex());
    smi->setValue("CoreApps", "AutoDetect", ui->autoDetect->isChecked());
    smi->setValue("CoreApps", "IconViewIconSize", QSize(ui->iconViewIconSize->currentText().toInt(), ui->iconViewIconSize->currentText().toInt()));
    smi->setValue("CoreApps", "ListViewIconSize", QSize(ui->listViewIconSize->currentText().toInt(), ui->listViewIconSize->currentText().toInt()));
    smi->setValue("CoreApps", "ToolsIconSize", QSize(ui->toolsIconSize->currentText().toInt(), ui->toolsIconSize->currentText().toInt()));
    smi->setValue("CoreApps", "DisableTrashConfirmationMessage", ui->disableTrashConfirmationMessage->isChecked());

    CPrime::SystemDefaultApps::setDefaultApp(CPrime::Terminal, ui->terminals->text());
	CPrime::SystemDefaultApps::setDefaultApp(CPrime::FileManager, ui->fileManger->text());
	CPrime::SystemDefaultApps::setDefaultApp(CPrime::ImageEditor, ui->imageEditor->text());
	CPrime::SystemDefaultApps::setDefaultApp(CPrime::MetadataViewer, ui->metadataViewer->text());
	CPrime::SystemDefaultApps::setDefaultApp(CPrime::SearchApp, ui->searchApp->text());
	CPrime::SystemDefaultApps::setDefaultApp(CPrime::BatchRenamer, ui->renamerApp->text());

    // coreaction
    QStringList pluginsFolders;
    for (int i = 0; i < ui->pluginPaths->count(); i++) {
        pluginsFolders << ui->pluginPaths->item(i)->text();
    }
    smi->setValue("CoreAction", "PluginsFolders", pluginsFolders);


    QStringList selectedPlugins;
    for (int i = 0; i < ui->pluginsList->count(); i++) {
        if (ui->pluginsList->item(i)->checkState() == Qt::Checked){
            selectedPlugins << ui->pluginsList->item(i)->data(Qt::UserRole).toString();
        }
    }
    smi->setValue("CoreAction", "SelectedPlugins", selectedPlugins);

    // coreterminal
    smi->setValue("CoreTerminal", "ColorScheme", ui->colorSchemesCB->currentText());
    smi->setValue("CoreTerminal", "KeyTab", ui->keytabCB->currentText());
    smi->setValue("CoreTerminal", "Opacity", ui->opacitySB->value());
    smi->setValue("CoreTerminal", "Font", ui->terminalFont->font());
    smi->setValue("CoreTerminal", "Shell", ui->shellLE->text());
    smi->setValue("CoreTerminal", "CursorShape", ui->cursorShapeCB->currentIndex());
	smi->setValue("CoreTerminal", "TERM", ui->termEnv->currentText());
	if (ui->limitHistorySize->isChecked()) {
		smi->setValue("CoreTerminal", "HistorySize", ui->historySB->value());
	} else {
		smi->setValue("CoreTerminal", "HistorySize", -1);
    }
	smi->setValue("CoreTerminal", "Rows", ui->sizeRowSB->value());
	smi->setValue("CoreTerminal", "Columns", ui->sizeColSB->value());

    // corestuff
    smi->setValue("CoreStuff", "Background", ui->bgPath->text());
    smi->setValue("CoreStuff", "WallpaperPositon", ui->wallpaperPos->currentIndex());
    smi->setValue("CoreStuff", "DockKeyCombo", ui->keySequenceEdit->keySequence().toString());

    // inform the user
	CPrime::MessageEngine::showMessage( "cc.cubocore.CoreGarage", "CoreGarage", "Settings Applied", "All CoreApps needs to restart to see the effect");
}

void coregarage::on_cancel_clicked()
{
    close();
}

void coregarage::on_ok_clicked()
{
    ui->cancel->setText("Close");

    saveSettings();
}

void coregarage::on_general_clicked()
{
    pageClick(ui->general, 0, tr("General"));
}

void coregarage::on_coreaction_clicked()
{
    pageClick(ui->coreaction, 1, tr("CoreAction"));
}

void coregarage::on_coreterminal_clicked()
{
    pageClick(ui->coreterminal, 2, tr("CoreTerminal"));
}

void coregarage::on_others_clicked()
{
    pageClick(ui->others, 3, "Others");
}

void coregarage::pageClick(QToolButton *btn, int i, const QString& title)
{
    // first do this to get all the space for contents
    if (uiMode == 2){
        ui->sideView->setVisible(false);
    }

    // all button checked false
    QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
    Q_FOREACH (auto b, toolBtns) {
        if (b) {
            b->setChecked(false);
        }
    }

    btn->setChecked(true);
    ui->selectedsection->setText(title);
    ui->pages->setCurrentIndex(i);
    this->setWindowTitle(title + " - CoreGarage");
}

// ---------------------- CoreAction page functionality ----------------

void coregarage::loadPlugins()
{
	allPlugins.clear();

	Q_FOREACH (QString path, m_pluginsFolders) {
		QDir pluginInfoDir(path);
		Q_FOREACH( QFileInfo pluginSoInfo, pluginInfoDir.entryInfoList({"*.so"}, QDir::Files)) {
			if (allPlugins.contains(pluginSoInfo.absoluteFilePath())) {
				continue;
			}

            QPluginLoader loader(pluginSoInfo.absoluteFilePath());
			auto pObject = loader.instance();

			if (pObject) {
				WidgetsInterface *plugin = qobject_cast<WidgetsInterface*>(pObject);
				if (plugin) {
					allPlugins[pluginSoInfo.absoluteFilePath()] = PluginInfo {plugin->name(), plugin->version()};
				}
			}
		}
	}

	sortCheckedAtFirst();
}

void coregarage::loadPluginsPaths()
{
    m_pluginsFolders.clear();

    m_pluginsFolders = smi->getValue("CoreAction", "PluginsFolders");

    if (not m_pluginsFolders.contains(m_defaultPluginPath)) {
        m_pluginsFolders << m_defaultPluginPath;
    }

    ui->pluginPaths->clear();
    Q_FOREACH (QString pFolder, m_pluginsFolders) {
        auto item = new QListWidgetItem(pFolder, ui->pluginPaths);
		item->setData(Qt::UserRole, pFolder);
		item->setIcon(QIcon::fromTheme("folder"));

        ui->pluginPaths->addItem(item);
    }

    ui->pluginPaths->setCurrentRow(0);

}

void coregarage::on_addPluginPath_clicked()
{
    QString p = QFileDialog::getExistingDirectory(this, "Select plugin location");

    if (p.isNull() || p.isEmpty()) {
        return;
    }

	auto item = new QListWidgetItem(QIcon::fromTheme("folder"), p);
    item->setData(Qt::UserRole, p);
    ui->pluginPaths->addItem(item);

	m_pluginsFolders.append(p);

	loadPlugins();
}

void coregarage::on_delPluginPath_clicked()
{
    if (ui->pluginPaths->currentItem()) {
        QListWidgetItem *it = ui->pluginPaths->takeItem(ui->pluginPaths->currentIndex().row());
		QString path = it->data(Qt::UserRole).toString();
		m_pluginsFolders.removeAll(path);

		delete it;
    }

	loadPlugins();
}

void coregarage::on_movePluginUp_clicked()
{
	auto selectedItems = ui->pluginsList->selectedItems();
	if (not selectedItems.count())
		return;

	auto item = selectedItems.at(0);
	if (item->checkState() == Qt::Checked) {
		int currIndex = ui->pluginsList->currentRow();
		if (currIndex > 0) {
			item = ui->pluginsList->takeItem(currIndex);
			ui->pluginsList->insertItem(currIndex - 1, item);
		}
	}
}

void coregarage::on_movePluginDown_clicked()
{
	auto selectedItems = ui->pluginsList->selectedItems();
	if (not selectedItems.count())
		return;

	auto item = selectedItems.at(0);
	if (item->checkState() == Qt::Checked) {
		int currIndex = ui->pluginsList->currentRow();
		if (currIndex > 0 and currIndex < ui->pluginsList->count() - 1) {
			item = ui->pluginsList->takeItem(currIndex);
			ui->pluginsList->insertItem(currIndex + 1, item);
			ui->pluginsList->setCurrentItem(item);
		}
	}
}

void coregarage::sortCheckedAtFirst()
{
	QStringList selectedPlugins = smi->getValue("CoreAction", "SelectedPlugins");

	ui->pluginsList->clear();
	Q_FOREACH (QString plugin, selectedPlugins) {
		if (not allPlugins.contains(plugin))
			continue;

		auto pluginInfo = allPlugins[plugin];

		auto item = new QListWidgetItem(QIcon::fromTheme("plugins"), pluginInfo.name + " " + pluginInfo.version);
		item->setData(Qt::UserRole, plugin);
		item->setFlags( Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable );
		item->setCheckState(Qt::Checked);

		ui->pluginsList->addItem(item);
	}

	Q_FOREACH (QString plugin, allPlugins.keys()) {
		if (selectedPlugins.contains(plugin))
			continue;

		auto pluginInfo = allPlugins[plugin];

		auto item = new QListWidgetItem(QIcon::fromTheme("plugins"), pluginInfo.name + " " + pluginInfo.version);
		item->setData(Qt::UserRole, plugin);
		item->setFlags( Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable );
		item->setCheckState(Qt::Unchecked);

		ui->pluginsList->addItem(item);
	}
}

void coregarage::on_setTerminals_clicked()
{
    // Select application in the dialog
    CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog( listViewIconSize, this );

    if (dialog->exec()) {
        if (dialog->getCurrentLauncher().compare("") != 0) {
            ui->terminals->setText(dialog->getCurrentLauncher());
        }
    }
}

void coregarage::on_setFileManger_clicked()
{
    // Select application in the dialog
    CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog( listViewIconSize, this );

    if (dialog->exec()) {
        if (dialog->getCurrentLauncher().compare("") != 0) {
            ui->fileManger->setText(dialog->getCurrentLauncher());
        }
    }
}

void coregarage::on_setImageEditor_clicked()
{
    // Select application in the dialog
    CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog( listViewIconSize, this );

    if (dialog->exec()) {
        if (dialog->getCurrentLauncher().compare("") != 0) {
            ui->imageEditor->setText(dialog->getCurrentLauncher());
        }
    }
}

void coregarage::on_setMetadataViewer_clicked()
{
    // Select application in the dialog
    CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog( listViewIconSize, this );

    if (dialog->exec()) {
        if (dialog->getCurrentLauncher().compare("") != 0) {
            ui->metadataViewer->setText(dialog->getCurrentLauncher());
        }
    }
}

void coregarage::on_setSearchApp_clicked()
{
    // Select application in the dialog
    CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog( listViewIconSize, this );

    if (dialog->exec()) {
        if (dialog->getCurrentLauncher().compare("") != 0) {
            ui->searchApp->setText(dialog->getCurrentLauncher());
        }
    }
}

void coregarage::on_setRenamerApp_clicked()
{
    // Select application in the dialog
    CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog( listViewIconSize, this );

    if (dialog->exec()) {
        if (dialog->getCurrentLauncher().compare("") != 0) {
            ui->renamerApp->setText(dialog->getCurrentLauncher());
        }
    }
}

void coregarage::showSideView()
{
    if (ui->sideView->isVisible())
        ui->sideView->setVisible(0);
    else
        ui->sideView->setVisible(1);
}

void coregarage::on_terminalFont_clicked()
{
    bool ok;
    QFont tFont = QFontDialog::getFont(&ok, smi->getValue("CoreTerminal", "Font"), this, "Choose Terminal Font", QFontDialog::MonospacedFonts);

    if (ok) {
        ui->terminalFont->setFont(tFont);
        ui->terminalFont->setText(tFont.family() + ", " + QString::number(tFont.pointSize()));
    }
}

void coregarage::on_setBG_clicked()
{
    ui->bgPath->setText(QFileDialog::getOpenFileName(this, "Select Baackground Path for CoreStuff", QDir::homePath()));
}

void coregarage::on_restoreDefault_clicked()
{
    QString msg = QString("All settings will be replaced by default settings.\nDo you want to continue?");
    QMessageBox message(QMessageBox::Question, "Restore Default", msg, QMessageBox::Yes | QMessageBox::No, this);
    message.setWindowIcon(QIcon::fromTheme("coregarage"));

    int reply = message.exec();
    if (reply == QMessageBox::No) {
        return;
    }

	QFile settingsFile(smi->defaultSettingsFilePath());
    settingsFile.remove();

	delete smi;
	smi = new settings;

    QTimer::singleShot(1000, this, &coregarage::initialize);
	CPrime::MessageEngine::showMessage("cc.cubocore.CoreGarage", "CoreGarage", "Restore successful", "Settings restore successfully to defaults.");
}

void coregarage::on_help_clicked()
{
    QString msg = QString("All Settings, Activites, Pins, Alarms, Reminders \nWill be backed up or restored");
    QMessageBox message(QMessageBox::Information, "Backup and Restore", msg, QMessageBox::Ok, this);
    message.setWindowIcon(QIcon::fromTheme("coregarage"));
    message.exec();
}


void coregarage::on_appTitle_clicked()
{
    showSideView();
}

void coregarage::on_menu_clicked(bool checked)
{
    Q_UNUSED(checked)
    showSideView();
}

void coregarage::on_autoDetect_clicked(bool checked)
{
    if (checked) {
        ui->uiMode->setEnabled(false);
        ui->autoDetect->setChecked(true);
    } else {
        ui->uiMode->setEnabled(true);
        ui->autoDetect->setChecked(false);
    }
}

